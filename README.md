# Gitlab-CI | Deploiement en Prod et Staging

<p align="center">
Please find the specifications by clicking
  <a href="https://github.com/eazytrainingfr/alpinehelloworld.git" alt="Crédit : eazytraining.fr" >
  </a>
</p>
------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


<img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 

LinkedIn : https://www.linkedin.com/in/carlinfongang/


## Objectif
Nous allons 
1. Créer un compte Heroku Cloud
2. Créer un token API sur le nouveau compte Heroku
3. Créer un variable d'environnement `HEROKU_API_KEY`
4. Scripté deux nouveaux jobs pour le déploiement de container en prod et staging
5. Nous allons contraindre des deploiement uniquement sur la branche principale

````
doc : https://devcenter.heroku.com/articles/container-registry-and-runtime
````

## Création du compte heroku
>![Alt text](img/image.png)

## Création d'un token API dépuis heroku
`login Heroku > Acount Settings > API Key`
>![Alt text](img/image-1.png)

## Ajout de la variable à GitLab
`login GitLab account > Settings > CICD > Variables > Add variables`
1. Remplir le champs "Key" par la nom de la variable 
2. Remplir le champs "Value" par la valeur de la clé API récupéré du compte heroku
3. valider 
>![Alt text](img/image-2.png)


## Création des app webapp-staging et webapp-prod sur heroku
`Boutton New > Create new app > Create app`
>![Alt text](img/image-4.png)
>![Alt text](img/image-3.png)

## Ajout des liens des webapp staging et prod à la variable `DEPLOY_LINK` sur GitLab
depuis heroku, pour chaque app, recupérer les liens en cliquant sur l'app
`webapp-staging > settings > Domains`
>![Alt text](img/image-6.png)

### Ajout des liens à la variable `DEPLOY_LINK`
`GitLab > CICD > Variables > Add variable` (opération à réaliser pour chacun des liens)
bien faire attention à faire correspondre les liens aux environnements prod ou staging
>![Alt text](img/image-5.png)

## Ajout des stage deploy staging et deploy prod au code
### deploy staging
>![Alt text](img/image-7.png)

### deploy prod
>![Alt text](img/image-8.png)

## Pipeline de déploiement fonctionnel
>![Alt text](img/image-9.png)

## Container pushé et disponible chez le provider hiruku
>![Alt text](img/image-10.png)

## Webapp-staging et Webapp-prod fonctionnel 
>![Alt text](img/image-11.png)
>![Alt text](img/image-12.png)
![Alt text](img/image-13.png)